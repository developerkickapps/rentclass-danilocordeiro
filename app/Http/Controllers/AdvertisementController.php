<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Http\Requests\AdvertisementRequest;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $advertisements = Advertisement::all()->toArray();
      return response()->json($advertisements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisementRequest $request)
    {
      $advertisement = new Advertisement();
      $advertisement->uuid = Uuid::uuid4();
      $advertisement->fill($request->all());
      $advertisement->save();

      return response()->json($advertisement, 201);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $advertisement)
    {
        $advertisementFiltrado = Advertisement::where('uuid', $advertisement)->first();
        if(!$advertisementFiltrado) {
            return response()->json([
                'message'   => 'Record not found',
            ], 404);
        }
        return response()->json($advertisementFiltrado, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertisementRequest $request, $advertisement)
    {
      $advertisementFiltrado = Advertisement::where('uuid', $advertisement)->first();
      if(!$advertisementFiltrado) {
          return response()->json([
              'message'   => 'Record not found',
          ], 404);
      }
      $advertisementFiltrado->fill($request->all());
      $advertisementFiltrado->save();

      return response()->json($advertisementFiltrado, 201);
    }

    public function publish(Request $request, $advertisement)
    {
      $advertisementFiltrado = Advertisement::where('uuid', $advertisement)->first();
      $advertisementFiltrado->published_at = Carbon::now();
      $advertisementFiltrado->save();

      return response()->json($advertisementFiltrado, 200);
    }

    public function search()
    {
      $advertisements = Advertisement::all()->toArray();
      return response()->json($advertisements);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $advertisement)
    {
        $advertisementFiltrado = Advertisement::where('uuid', $advertisement)->first();
        if(!$advertisementFiltrado) {
            return response()->json([
                'message'   => 'Record not found',
            ], 204);
        }
        $advertisementFiltrado->delete();

        return response()->json($advertisementFiltrado, 200);
    }
}
