<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
  protected $fillable = ['user_id', 'uuid', 'title', 'description', 'tags', 'price', 'published_at'];

  protected $hidden = ['id','password'];

  protected $dates = ['deleted_at'];

}
