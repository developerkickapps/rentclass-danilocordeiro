<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/', function () {
    return response()->json(['message' => 'Advertisements API', 'status' => 'Connected']);;
});

Route::resource('users', 'UserController', ['only' => [ 'index', 'store', 'show', 'update', 'destroy']]);

Route::resource('advertisements', 'AdvertisementController');
Route::post('advertisements/{advertisement}/toggle-published', 'AdvertisementController@publish');
Route::get('search', 'AdvertisementController@search');
Route::post('advertisements/{advertisement}/picture', 'PictureController@store');
